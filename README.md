About
=====

This repository includes my small tools on zsh.


how to use
=====

with [zplug](https://github.com/zplug/zplug),

```zsh
zplug "matsub/zshtools", from: bitbucket
```

## requirements

- dict
	- [ghq](https://github.com/motemen/ghq)
	- [fzf](https://github.com/junegunn/fzf)
